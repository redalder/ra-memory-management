
# License

This file is distributed under the BSD 3-Clause License.<br>
See LICENSE for details.<br>

# Classes
## Region
#### Fields

```int32_t _state``` - holds the size including 4 byte header, signing bit signifies free-ness<br>

#### Methods

```Region(int32_t size, bool free)``` - including 4 header<br>
```int32_t GetSize()``` - including 4 byte header<br>
```bool GetFree()```<br>
```void SetFree(bool free)```<br>
```Region* NextRegion()```<br>
```void* Data()```<br>
 
## Pool
#### Fields

```void* _start``` - points to the start of heap allocated memory this `Pool` manages<br>
```Region* _region``` - points to the same location as `_start`<br>
```int32_t _size``` - including all the necessary headers<br>
```Pool* _nextPool``` - initialized to `nullptr`<br>

#### Methods

```Pool(void* start, int32_t size)``` - raw size<br>
```Pool(int32_t&& size)```<br>
```~Pool()```<br>

```Region* Allocate(int32_t size)``` - including 4 byte header<br>
```void Deallocate(Region* region)``` - does not run destructor<br>
```int32_t NumberOfRegions()``` - excluding end `Region`<br>
```int32_t NumberOfSubPools()``` - excluding master `Pool`<br>
```void Optimize()``` - merges free regions and deletes free pools<br>
```void MergeFreeRegions()```<br>
```void DeleteFreePools()```<br>
```bool inline IsFree()```<br>

```void split(Region* region, int32_t first, int32_t second)```<br>

#### Accessor

# Template

```template<class T, bool destruct>``` - instructs the wrapper if it should destruct it's internal type

# Fields

```Region* _region```

# Methods

```explicit Accessor(Region* region)```
```explicit Accessor(Pool& pool)```
```~Accessor()```

```T& Get()```
