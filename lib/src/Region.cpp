//===----------------------------------------------------------------------===//
//
//                          Red Adler Memory Management
//
// This file is distributed under the BSD 3-Clause License.
// See LICENSE for details.
//
//===----------------------------------------------------------------------===//

#include <cmath>
#include <RedAdler/MemoryManagement/Region.h>


#include "RedAdler/MemoryManagement/Region.h"

Region::Region(int32_t size, bool free) {
    _state = free ? abs(size) * -1 : abs(size);
}

int32_t Region::GetSize() {
    return abs(_state);
}

bool Region::GetFree() {
    return (_state / abs(_state)) == -1 ? true : false;
}

void Region::SetFree(bool free) {
    _state = free ? abs(_state) * -1 : abs(_state);
}

Region* Region::NextRegion() {
    return (Region*) ((char*)this + abs(_state));
}

void* Region::Data() {
    return this + 1;
}
