//===----------------------------------------------------------------------===//
//
//                          Red Adler Memory Management
//
// This file is distributed under the BSD 3-Clause License.
// See LICENSE for details.
//
//===----------------------------------------------------------------------===//

#include <cstdlib>
#include <RedAdler/MemoryManagement/Pool.h>


#include "RedAdler/MemoryManagement/Pool.h"

Pool::Pool(void* start, int32_t size) {
    if (size < sizeof(Region) * 3)
        ;//@TODO throw exception

    _start = start;
    _size = size;

    *_region = Region(size - sizeof(Region), true);
    *(Region*)((char*)_region + size - sizeof(Region)) = Region(sizeof(Region), false);
}

Pool::Pool(int32_t size) {
    if (size < sizeof(Region) * 3)
        ;//@TODO throw exception

    _start = malloc(size);
    _size = size;

    *_region = Region(size - sizeof(Region), true);
    *(Region*)((char*)_region + size - sizeof(Region)) = Region(sizeof(Region), false);
}

Pool::~Pool() {
    delete _nextPool;

    free(_start);
}

Region* Pool::Allocate(int32_t size) {
    Pool* selectedPool = this;

    if (size % sizeof(Region) || size < 8) {
        //@TODO throw exception
    }


    while (true) {
        Region* selectedRegion = selectedPool->_region;

        while (!(selectedRegion->GetSize() == sizeof(Region) && !selectedRegion->GetFree()) && selectedRegion->GetSize() != sizeof(Region)) {
            if (selectedRegion->GetSize() == size && selectedRegion->GetFree()) {
                selectedRegion->SetFree(false);

                return selectedRegion;
            } else if (selectedRegion->GetSize() > size && selectedRegion->GetFree()) {
                split(selectedRegion, size, selectedRegion->GetSize() - size);
                selectedRegion->SetFree(false);

                return selectedRegion;
            }

            selectedRegion = selectedRegion->NextRegion();
        }

        if (selectedPool->_nextPool == nullptr)
            selectedPool->_nextPool = new Pool(_size);

        selectedPool = selectedPool->_nextPool;
    }
}

void Pool::Deallocate(Region* region) {
    if ((char*)region >= (char*)_start && (char*)region < (char*)_start + _size) {
        region->SetFree(true);
    } else {
        if (_nextPool != nullptr) {
            _nextPool->Deallocate(region);
        }
    }
}

int32_t Pool::NumberOfRegions() {
    Region* selectedRegion = _region;
    uint32_t regions = 0;

    while (!(selectedRegion->GetSize() == sizeof(Region) && !selectedRegion->GetFree()) && selectedRegion->GetSize() != sizeof(Region)) {
        regions++;

        selectedRegion = selectedRegion->NextRegion();
    }

    return regions;
}

int32_t Pool::NumberOfSubPools() {
    int i = 1;

    Pool* selectedPool = _nextPool;

    while (selectedPool != nullptr) {
        i++;

        selectedPool = selectedPool->_nextPool;
    }

    return i;
}

void Pool::Optimize() {
    MergeFreeRegions();
    DeleteFreePools();
}

void Pool::split(Region* region, int32_t first, int32_t second) {
    if (region->GetFree() && region->GetSize() == first + second) {
        *region = Region(first, true);
        *region->NextRegion() = Region(second, true);
    } else {
        //@TODO throw exception
    }
}

void Pool::MergeFreeRegions() {
    Region* selectedRegion = _region;

    while (!(selectedRegion->GetSize() == sizeof(Region) && !selectedRegion->GetFree()) && selectedRegion->GetSize() != sizeof(Region)) {
        if (selectedRegion->GetFree() && selectedRegion->NextRegion()->GetFree()) {
            *selectedRegion = Region(selectedRegion->GetSize() + selectedRegion->NextRegion()->GetSize(), true);
        } else {
            selectedRegion = selectedRegion->NextRegion();
        }
    }
}

void Pool::DeleteFreePools() {
    if (_nextPool != nullptr) {
        Pool *previousPool = this;
        Pool *selectedPool = this->_nextPool;
        Pool *nextPool = this->_nextPool->_nextPool;

        while (selectedPool != nullptr) {
            if (selectedPool->IsFree()) {
                selectedPool->_nextPool = nullptr;
                delete selectedPool;

                previousPool->_nextPool = nextPool;

                selectedPool = nextPool;
                nextPool = selectedPool->_nextPool;
            } else {
                previousPool = selectedPool;
                selectedPool = nextPool;
                if (selectedPool != nullptr)
                    nextPool = selectedPool->_nextPool;
            }
        }
    }
}
