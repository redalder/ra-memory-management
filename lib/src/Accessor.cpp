//===----------------------------------------------------------------------===//
//
//                          Red Adler Memory Management
//
// This file is distributed under the BSD 3-Clause License.
// See LICENSE for details.
//
//===----------------------------------------------------------------------===//

#include "RedAdler/MemoryManagement/Accessor.h"
