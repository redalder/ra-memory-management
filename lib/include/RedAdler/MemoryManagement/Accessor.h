//===----------------------------------------------------------------------===//
//
//                          Red Adler Memory Management
//
// This file is distributed under the BSD 3-Clause License.
// See LICENSE for details.
//
//===----------------------------------------------------------------------===//

#ifndef RA_MEMORYMANAGEMENT_ACCESSOR_H
#define RA_MEMORYMANAGEMENT_ACCESSOR_H

#include "Region.h"
#include "Pool.h"

template <class T, bool destructor, bool mergeFree, bool deleteFree>
class Accessor {
public:
    explicit Accessor(Pool& pool) {
        _region = pool.Allocate(sizeof(T) + sizeof(Region));
        _pool = &pool;
    }

    ~Accessor() {
        if constexpr (destructor) {
            ((T*)(_region + 1))->~T();
            _pool->Deallocate(_region);
        } if constexpr (mergeFree) {
            _pool->MergeFreeRegions();
        } if constexpr (deleteFree) {
            _pool->DeleteFreePools();
        }
    }

    T& Get() {
        return *((T*)(_region + 1));
    }
protected:
private:
    Region* _region;
    Pool* _pool;
};


#endif //RA_MEMORYMANAGEMENT_ACCESSOR_H
