//===----------------------------------------------------------------------===//
//
//                          Red Adler Memory Management
//
// This file is distributed under the BSD 3-Clause License.
// See LICENSE for details.
//
//===----------------------------------------------------------------------===//

#ifndef RA_MEMORYMANAGEMENT_POOL_H
#define RA_MEMORYMANAGEMENT_POOL_H


#include <cstdint>

#include "Region.h"

class Pool {
public:
    Pool(void* start, int32_t size);
    explicit Pool(int32_t size);
    ~Pool();

    Region* Allocate(int32_t size);
    void Deallocate(Region* region);
    int32_t NumberOfRegions();
    int32_t NumberOfSubPools();
    void Optimize();
    void MergeFreeRegions();
    void DeleteFreePools();
    bool inline IsFree() {
        return NumberOfRegions() == 1 && _region->GetFree();
    }
protected:
private:
    union {
        void* _start;
        Region* _region;
    };
    int32_t _size;
    Pool* _nextPool = nullptr;

    void split(Region* region, int32_t first, int32_t second);
};


#endif //RA_MEMORYMANAGEMENT_POOL_H
