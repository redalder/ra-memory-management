//===----------------------------------------------------------------------===//
//
//                          Red Adler Memory Management
//
// This file is distributed under the BSD 3-Clause License.
// See LICENSE for details.
//
//===----------------------------------------------------------------------===//

#ifndef RA_MEMORYMANAGEMENT_REGION_H
#define RA_MEMORYMANAGEMENT_REGION_H


#include <cstdint>

class Region {
public:
    Region(int32_t size, bool free);

    int32_t GetSize();
    bool GetFree();
    void SetFree(bool free);
    Region* NextRegion();
    void* Data();
protected:
private:
    int32_t _state = 0;
};


#endif //RA_MEMORYMANAGEMENT_REGION_H
