//===----------------------------------------------------------------------===//
//
//                          Red Adler Memory Management
//
// This file is distributed under the BSD 3-Clause License.
// See LICENSE for details.
//
//===----------------------------------------------------------------------===//

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <RedAdler/MemoryManagement/Pool.h>
#include <RedAdler/MemoryManagement/Accessor.h>
#include <RedAdler/MemoryManagement/Region.h>

void checkRegion() {
    int i = 8;

    Region region = Region(i, true);

    std::cout <<    "Initial size: " << 8 << '\n' <<
              "Reported size: " << region.GetSize() << '\n' <<
              "Initial free-ness: " << true << '\n' <<
              "Reported free-ness: " << region.GetFree() << '\n' <<
              "Changing the free-ness to 0." << '\n';

    region.SetFree(false);
    
    std::cout <<    "Free-ness should now be 0, it is: " << region.GetFree() << '\n' <<
              "Reported size should be 8, it is: " << region.GetSize() << "\n\n";
}

void checkPool() {
    Pool pool = Pool(12);

    Region* region1 = pool.Allocate(8);
    Region* region2 = pool.Allocate(8);
    Region* region3 = pool.Allocate(8);
    Region* region4 = pool.Allocate(8);
    Region* region5 = pool.Allocate(8);

    *(int*)region1->Data() = 41;
    *(int*)region2->Data() = 42;
    *(int*)region3->Data() = 43;
    *(int*)region4->Data() = 44;
    *(int*)region5->Data() = 45;

    std::cout <<    "Created pool with the size of: " << 12 << '\n' <<
                    "Allocated " << 8 << " * " << 5 << " bytes, including the header" << '\n' <<
                    "Assigning " << 41 << " + [allocation_number] " << " as an int to all allocations" << '\n';

    std::cout <<    "Reading back the values of all allocations: " << *(int*)region1->Data() << ", " <<
                    *(int*)region2->Data() << ", " << *(int*)region3->Data() << ", " << *(int*)region4->Data() <<
                    ", " << *(int*)region5->Data() << '\n' <<
                    "Number of pools currently: " << pool.NumberOfSubPools() << '\n' <<
                    "Testing deletion of free pools with optimization..." << '\n';

    pool.Deallocate(region2);
    pool.Deallocate(region4);

    pool.Optimize();

    std::cout << "Number of pools after deletion: " << pool.NumberOfSubPools() << "\n\n";
}

void checkAccessor() {
    Pool pool = Pool(512);

    auto accessor = Accessor<int, false, true, true>(pool);

    std::cout <<    "Created pool with size of: " << 512 << '\n' <<
                    "Created accessor of type int without auto-destruction" << '\n' <<
                    "Setting it to: " << 45 << '\n';

    accessor.Get() = 45;

    std::cout <<    "Value of accessor is now: " << accessor.Get() << '\n';

    auto accessor1 = Accessor<std::string, true, true, true>(pool);

    std::cout <<    "Created another accessor of type std::string with auto-destruction" << '\n' <<
                    "Setting it to: " << "\"Hello, world!\"" << '\n';

    accessor1.Get() = "Hello, world!";

    std::cout <<    "Accessors value is now: " << accessor1.Get() << "\n\n";
}

int main() {
    checkRegion();
    checkPool();
    checkAccessor();


    return 0;
}
